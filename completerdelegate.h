#ifndef COMPLETERDELEGATE_H
#define COMPLETERDELEGATE_H

#include <QAbstractItemDelegate>

class CompleterDelegate : public QAbstractItemDelegate
{
public:
    CompleterDelegate(QObject* parent = 0);

    void paint(QPainter *painter, const QStyleOptionViewItem &option, const QModelIndex &index) const;
    QSize sizeHint(const QStyleOptionViewItem &option, const QModelIndex &index) const;
};

#endif // COMPLETERDELEGATE_H
