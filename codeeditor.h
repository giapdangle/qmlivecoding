#ifndef CODEEDITOR_H
#define CODEEDITOR_H

#include <QObject>
#include <QHBoxLayout>
#include <QTextEdit>
#include <QSortFilterProxyModel>
#include <highlighter.h>
#include <completer.h>
#include <completionmodel.h>
#include <editor.h>

class CodeEditor : public QObject
{
    Q_OBJECT
public:
    explicit CodeEditor(QWidget *parent = 0);
    
signals:
    
public slots:
    void load();
    void exportFile();
    void importFile();

private slots:
    void save();
    void updateCompleter();
    void insertSnippet(const QString& snippet);

public:
    QSortFilterProxyModel* filterModel;
    CompletionModel completionModel;
    Completer* completer;
    QWidget window;
    QHBoxLayout layout;
    Editor textEditor;
    Highlighter highlighter;    
};

#endif // CODEEDITOR_H
