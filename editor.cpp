#include "editor.h"
#include <QKeyEvent>
#include <QTextBlock>

Editor::Editor(QWidget *parent) : QTextEdit(parent)
{
}

void Editor::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_BraceRight)
    {
        QTextCursor cursor = textCursor();
        int position = cursor.position();
        if ((position > 0) && (cursor.block().text()[position - 1] == '\t'))
            textCursor().deletePreviousChar();
    }

    QTextEdit::keyPressEvent(e);
}

void Editor::keyReleaseEvent(QKeyEvent *e)
{
    QTextEdit::keyReleaseEvent(e);
    if (e->key() == Qt::Key_Return || e->key() == Qt::Key_Enter)
    {
        insertPlainText(tabs());
    }    
}

QString Editor::tabs() const
{
    int currentLine = currentTextLine();
    QStringList lines = toPlainText().split("\n");

    while (--currentLine >= 0)
    {
        QString line = lines[currentLine];
        if (line.trimmed().isEmpty())
            continue;

        int pos = 0;
        while (pos <= line.length() && line[pos] == '\t')
            pos++;


        if (line.trimmed().right(1) == "{")
            pos++;

        return QString(pos, '\t');
    }

    return QString();
}

int Editor::currentTextLine() const
{
    return toPlainText().left(textCursor().position()).count("\n");
}
